[Preposições](#preposições)  
[Terminações de caso](#terminações-de-caso)

# Terminações de Caso
Em Latim, substantivos e adjetivos têm códigos especiais chamados de terminações de caso para mostrar o trabalho que eles fazem na sentença.  

## Sujeito (Fazendo a ação)
_Caso nominativo_  
Código: `a`  

> agricol`a`  
> Fazendeiro

## Objeto (sofrendo a ação)
_Caso acusativo_  
Código: `am`  

> puell`am` amat  
> Ele ama a menina

## Um exemplo simples

> agricol`a` puell`am` amat  
> O fazendeiro ama a menina

## Ordem de tradução
1. Nominativo
2. Verbo
3. Acusativo  

**Nota bene**: Nunca traduzir o acusativo antes do verbo. Se não tem nominativo, vá direto para o verbo.   

> cas`am` videmus  
> Nós vemos a casa

Fonte: [Aplicativo Learn Basic Latin](https://play.google.com/store/apps/details?id=com.mra.one.free)

---

# Preposições
## Frases preposicionais
Preposições são pequenas e conectivas palavras que normalmente nos dizem **onde** algo ocorreu  
> O menino pulou `embaixo` da cama  

Em Latim, preposições sempre têm um caso particular. Você tem que aprender qual caso cada preposição usa.  

Algumas preposições, como `ad` (para) são sempre seguidas de um acusativo.  

> `ad` vill`am`  
> para a casa

Outras, como `ex` (fora de), tomam o caso ablativo.  

> `ex` vill`a`  
> fora da casa

## Ordem de tradução
Preposições e seus substantivos podem ser traduzidos em qualquer momento na sentença.  

### Sumário de casos

|Caso|Função|Sufixo|
|---|---|---|
|Nominativo|Sujeito|vill`a`|
|Acusativo|Objeto ou após algumas preposições|vill`am`|
|Ablativo|Após algumas preposições|vill`a`|  


Fonte: [Aplicativo Learn Basic Latin](https://play.google.com/store/apps/details?id=com.mra.one.free)